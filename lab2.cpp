#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    //declaring variables
    double celsius, fahrenheit;
    double radius, circumference, cArea;
    double length, width, recArea;
    int option;

    cout << "Welcome To My Calculator" << endl;

    do
    {
    //display menu
    cout << "MENU" << endl;
    cout << "------------------------------------"
         << "\n1- Convert Celsius to Fahrenheit"
         << "\n2- Convert Fahrenheit to Celsius"
         << "\n3- Calculate Circumference of a Circle"
         << "\n4- Calculate Area of a Circle"
         << "\n5- Calculate Area of a Rectangle"
         << "\n6- Calculate Area of a Triangle"
         << "\n7- Calculate Volume of a Cylinder"
         << "\n8- Calculate Volume of a Cone"
         << "\n9- Quit Program" << endl;

    //select option
    cout << "\nEnter option: ";
    cin >> option;
    cout << endl;
    while(option<1 || option>9)
    {
        cout << "Invalid input! Please enter again." << endl;
        cout << endl;
        cout << "Enter option: ";
        cin >> option;
        cout << endl;
    }

    //switch option
    switch(option)
    {
    case 1:
        {
            cout << "Enter Celsius value to convert to Fahrenheit: ";
            cin >> celsius;

            fahrenheit = (celsius * 9.0) / 5.0 + 32;
            cout << "\nFahrenheit value is: " << fahrenheit << endl;
            cout << endl;
            break;
        }
    case 2:
        {
            double cel, fah;
            cout << "Enter Fahrenheit value to convert to Celsius: ";
            cin >> fah;

            cel = (fah - 32) * (5/9);
            cout << "\nCelsius value is: " << cel << endl;
            cout << endl;
            break;
        }
    case 3:
        {
            const float PI = 3.14159;
            cout << "Enter the radius of the Circle: ";
            cin >> radius;

            circumference = 2*PI*radius;
            cout << "\nCircumference value is: " << circumference << endl;
            cout << endl;
            break;
        }
    case 4:
        {
            double rad;
            const float PI = 3.14159;
            cout << "Enter the radius of the Circle: ";
            cin >> rad;

            cArea = (rad*rad)*PI;
            cout << "\nArea value is: " << cArea << endl;
            cout << endl;
            break;
        }
    case 5:
        {
            cout << "Enter the Length of the Rectangle: ";
            cin >> length;
            cout << "\nEnter the Width of the Rectangle: ";
            cin >> width;

            recArea = length*width;
            cout << "\nArea value is: " << recArea << endl;
            cout << endl;
            break;
        }
    case 6:
        {
            //declaring triangles variables
            float side1, side2, side3, area, s;

            //prompt user for triangle values
            cout << "Enter length of 1st side of the triangle : ";
            cin >> side1;

            cout << "Enter length of 2nd side of the triangle : ";
            cin >> side2;

            cout << "Enter length of 3rd side of the triangle : ";
            cin >> side3;

            s = (side1+side2+side3)/2;

            area = sqrt(s*(s-side1)*(s-side2)*(s-side3));

            cout << " The area of the triangle is : " << area << endl;
            cout << endl;
            break;
        }
    case 7:
        {
            //declaring variables
            int rad1,hgt;
            float volcy;

            //prompt user for cylinder values
            cout << "Enter radius of the Cylinder : ";
            cin >> rad1;

            cout << "Enter height of the Cylinder : ";
            cin >> hgt;

            volcy=(3.14*rad1*rad1*hgt);
            cout << "The volume of a cylinder is : " << volcy << endl;
            cout << endl;
            break;
        }
    case 8:
        {
            //Declares variables
            float r, h, v;
            float pi = 3.14159;

            //prompt user for cone's values
            cout << "Input cone's radius.";
            cin >> r;

            cout << "Input cone's height.";
            cin >>  h;

            v = (1.0/3.0) * pi * (r*r) * h;

            cout << "The volume of the cone is\n\n " << v << "\n";
            break;
        }
    case 9:
        {
            system("exit");
            break;
        }
    }

    }while(option!=9);

return 0;

}
